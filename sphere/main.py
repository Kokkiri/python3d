from PIL import Image
from math import cos, sin, pi

width = 500
height = 500
WHITE = (255, 255, 255)

img = Image.new('RGB', (width, height), WHITE)


class Circle():
    def __init__(self, posx, posy, r, color):
        angle = (x for x in range(r*r))

        for agl in angle:
            o = round(sin(agl) * r)
            a = round(cos(agl) * r)
            if int(posx+a) <= 0 or int(posy+o) <= 0 or int(posx+a) >= 500 or int(posy+o) >= 500:
                pass
            else:
                img.putpixel((round(posx+a), round(posy+o)), color)

yellow_circle2 = Circle(400, 350, 100, (255, 255, 0))
blue_circle3 = Circle(250, 250, 150, (0, 0, 255))
red_circle = Circle(50, 0, 300, (255, 0, 0))

# img.show()
img.save('result.png')

# img = Image.new('P', (420, 420), (255, 255, 255))
# img = Image.open("image.jpg")
# img.resize((width, height)).crop((40, 40, 480, 480)).save('image.jpg')
# width, height = img.size
# px = img.load()
# img.putpixel((x, y), (0, 0, 0))

from PIL import Image
from random import randint

def gen_matrice():
    ################################ CROP THE IMAGE ################################

    _img = Image.open("maze-labyrinth-vector-illustration-260nw-365203412.webp")
    # _img = Image.open("toto.png")
    _px = _img.load()
    _width, _height = _img.size

    tab = []
    for x in range(_width):
        for y in range(_height):
            if _px[x, y][0] < 20:
                tab.append((x, y))

    _img.resize((_width, _height)).crop((tab[0][0], tab[0][1], tab[-1][0]+1, tab[-1][1]+1)).save('image.jpg')

    ################################################################################

    img = Image.open("image.jpg")
    px = img.load()
    width, height = img.size
    print("lougueur:", width, "largeur:", height)

    def get_cell_size():
        line = []
        current_value = 100
        for x in range(width):
            if px[x, 5][0] < 200:
                line.append(x)
        for x in range(len(line)-1):
            new_value = line[x+1] - line[x]
            if current_value > new_value and new_value > 1:
                current_value = new_value
        return current_value

    print('taille cell img', get_cell_size())
    # sourcery skip
    total_cell_x = int((width - 6) / get_cell_size()*2)
    total_cell_y = int((height - 6) / get_cell_size()*2)

    print("total de cellule en x:", total_cell_x)
    print("total de cellule en y:", total_cell_y)

    cell_size = width / total_cell_x

    print("taille des cellules:", cell_size)


    def one_cell(x_img, y_img, cell_x, cell_y):
        for x in range(cell_x):
            for y in range(cell_y):
                if px[x+x_img, y+y_img][0] < 200:
                    return 1
        return 0

    total_line = cell_size

    matrice = [[1] * (total_cell_x+1)]
    for line in range(total_cell_y - 1):
        val_line = round(line * cell_size)
        cell_y = round(total_line - val_line)
        total_col = cell_size
        matrice_line = [1]
        for col in range(total_cell_x - 1):
            val_col = round(col * cell_size)
            cell_x = round(total_col - val_col)
            x_img = round(3 + cell_size * col)
            y_img = round(3 + cell_size * line)
            matrice_line.append(one_cell(x_img, y_img, cell_x, cell_y))
            total_col += cell_size
        matrice_line.append(1)
        matrice.append(matrice_line)
        total_line += cell_size
    matrice.append([1]*(total_cell_x+1))
    print(matrice)
    return matrice
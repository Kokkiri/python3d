ce programme permet de convertir l'image d'un labyrinthe en matrice et de trouver la solution.

## execution
    python3 labyrinthe.py

## description
handle_img_lab.py converti l'image en matrice.<br/>
labyrinthe.py résout le labyrinthe.<br/>

#### handle_img_lab.py
sur une ligne je récupère le nombre de pixel noir et leurs coordonnées. je détermine la distance la plus courte entre deux pixels et en déduit la taille d'une cellule.<br/>

par dessus le labyrinthe je "dessine" une grille dont chaque cellule détermine si c'est un chemin ou un mur qui se trouve en arrière plan.<br/>
Si c'est un mur la fonction retourne 1, si c'est un chemin elle retourne 0.<br/>
Ces données sont stockées dans des tableaux et on obtiens une matrice.

#### labyrinthe.py
j'initialize la cellule de départ et la cellule d'arrivée.<br/>
dans un ordre prédéfinie, le programme observe si les cellules autour de lui sont des 0 ou des 1.<br/>
À la première cellule marqué d'un 0, il s'y engage. Les cellules derrière lui sont marquées d'un 2 pour indiquer qu'il est déjà passer par là.<br/>
Quand c'est un cul de sac, le programme retourne ne arrière et marque les cellules derrière lui d'un 3 pour indiquer qu'il ne repassera pas par là.<br/>
À chaque numéro a été attribué une couleur et le processus a été animé.
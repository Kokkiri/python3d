from itertools import product as comb

nbd = int(input("nombre de des: "))
somme = int(input("somme à obtenir: "))
count = 0

for t in comb(range(1,7), repeat=nbd):
    if sum(t) == somme:
        print(t)
        count += 1
print(count)

from random import randint, random

def listAleaFloat(n):
    return [random() for i in range(n)]

def call_list():
    n = randint(2,100)
    list = listAleaFloat(n)
    print(list)
    print("amplitude: ", max(list) - min(list))
    print("moyenne: ", sum(list)/n)

call_list()

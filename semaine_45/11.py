def valide(ch):
    res = any(ch)
    for c in ch:
        res = res and c in "atgc"
    return res

def saisie(ch):
    s = input("{}:".format(ch))
    while not valide(s):
        s = input("La {} ne peut contenir que les chainons 'a' 'c' 'g' et 't'.".format(ch))
    return s

def proportion(a, s):
    return a.count(s)*100/(len(a)/len(s))

thousand = ["", "M", "MM", "MMM"]
hundred = ["", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM"]
ten = ["", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC",]
unit = ["", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX"]

def num(argument):
    string = str(argument)
    one = 0
    two = 0
    three = 0
    four = 0
    if len(string) == 4:
        four = str(unit[int(string[3])])
        three = str(ten[int(string[2])])
        two = str(hundred[int(string[1])])
        one = str(thousand[int(string[0])])
        print(one+two+three+four)
    if len(string) == 3:
        three = str(unit[int(string[2])])
        two = str(ten[int(string[1])])
        one = str(hundred[int(string[0])])
        print(one+two+three)
    if len(string) == 2:
        two = str(unit[int(string[1])])
        one = str(ten[int(string[0])])
        print(one+two)
    if len(string) == 1:
        one = str(unit[int(string[0])])
        print(one)

argument = int(input("give a number between 1 and 3999: "))

num(argument)

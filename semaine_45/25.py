# factoriel

import sys

def factoriel(n):
    if n < 2:
        return 1
    else:
        return n * factoriel(n-1)

if __name__ == '__main__':
    try:
        print(factoriel(int(sys.argv[1])))
    except ValueError:
        print('passer un entier en argument.')
    except IndexError:
        print('passer un argument. ex: python3 25.py 5')
# cette fonction optimise la money rendu en billet de 10, 5 ou pièce de 2
# ex: 34 euros vaut trois billets de 10 et deux pièces de 2
# resultat: {'ten': 3, 'five': 0, 'two': 2, 'rest': 0}


import sys

def render_cash(cash):

    money = {'ten': 0, 'five': 0, 'two': 0, 'rest': 0}

    if cash >= 10:
        rest = cash % 10
        ten = (cash - rest) // 10
        money['ten'] = ten
        cash = rest
    if cash >= 5:
        rest = cash % 5
        five = (cash - rest) // 5
        money['five'] = five
        cash = rest
    if cash >= 2:
        rest = cash % 2
        two = (cash - rest) // 2
        money['two'] = two
        cash = rest
    if cash > 0:
        money['rest'] = rest

    return money

if __name__ == '__main__':

    try:
        print(render_cash(int(sys.argv[1])))
    except IndexError:
        print('passez un nombre en argument. ex: python3 24.py 54')
    except ValueError:
        print('passez un entier en argument')


import sys
import xlrd
import csv

breakdown = xlrd.open_workbook("breakdown.xls")
worksheet = breakdown.sheet_by_name("Feuille1")

rows = worksheet.nrows
cols = worksheet.ncols

row = []
table = []
info = []

# liste des arguments de ligne de commande
# title = cellule de titre
# title_lines_num = le nombre de ligne des infos de titre
# table = cellule du tableau
# ex: py convert_with_xlrd.py 1 1 3 4 2
title_x = int(sys.argv[1])
title_y = int(sys.argv[2])
title_lines_num = int(sys.argv[3])
table_x = int(sys.argv[4])
table_y = int(sys.argv[5])



for i in range(title_lines_num):
    info.append(worksheet.cell(title_x+i, title_y).value)

for x in range(table_x, rows):
    for y in range(table_y, cols):

        if len(str(worksheet.cell(x,y).value)) == 0:
            row.append(False)
        elif worksheet.cell(x,y).value == "x":
            row.append(True)
        else:
            row.append(worksheet.cell(x,y).value)

    table.append(row)
    row = []



with open(f'{" ".join(info)}.csv', 'w', newline='') as csvfile:
    writer = csv.writer(csvfile)

    for line in table:
        writer.writerow(line)

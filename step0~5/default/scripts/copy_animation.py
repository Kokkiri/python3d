# get all node type transform
list = []
[list.append(el) for el in mc.ls(type='transform')]

# copy key object one to another and delete the copied object
def copy_paste(*args):
    # get the value of the selected object
    copy_obj = mc.textScrollList('copy_list', query=True, selectItem=True)
    paste_obj = mc.textScrollList('paste_list', query=True, selectItem=True)

    mc.copyKey(mc.ls(copy_obj), time=(0, 30))
    mc.pasteKey(mc.ls(paste_obj))
    mc.delete(mc.ls(copy_obj))

# create window
mc.window()
mc.columnLayout()
mc.text('select object to copy')
mc.textScrollList( 'copy_list', numberOfRows=8, allowMultiSelection=False,
			append=list)
mc.text('select object to paste on')
cmds.textScrollList( 'paste_list', numberOfRows=8, allowMultiSelection=False,
			append=list)

mc.button('Copy', command=copy_paste)
mc.showWindow()

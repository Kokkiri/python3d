selection = mc.ls(selection=True)

# define animation
def set_animation(currentTime, pos_z, object):
    mc.currentTime(currentTime)
    mc.move(0, 0, pos_z, object, relative=True)
    mc.setKeyframe(object)


set_animation(0, 0, selection)
set_animation(30, 5, selection)

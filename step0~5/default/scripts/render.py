import maya.mel

mc.createRenderLayer(mc.ls(selection=True), name='Layer1')

mc.editRenderLayerAdjustment( 'lambert1.color', 'mib_amb_occlusion1.outValue', layer='layer1' )

mc.connectAttr( 'lambert1.color', 'mib_amb_occlusion1.outValue', force=True)

maya.mel.eval("BatchRender;")

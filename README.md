## userSetup
locate in C:\Users\my_name\Documents\maya\scripts<br/>
import cmds as mc

### Liste de compréhension
ex: r = [l for l in list if l > 100]

### Opérateur ternaire
ex: print("pair" if nb%2 == 0 else "impair")

### fonctions
any()<br/>
count()<br/>
zip()

### library
itertools
